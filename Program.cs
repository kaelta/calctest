﻿namespace Program
{

    class Program
    {
        private static readonly Dictionary<string, Func<int, int, int>> Answers = new()
        {
            {"+", (int x, int y) => x + y},
            {"-", (int x, int y) => x - y},
            {"/", (int x, int y) => x / y},
            {"*", (int x, int y) => x * y}
        };

        private static string GetQuestion()
        {
            string question = Console.ReadLine() ?? "";
            if (!string.IsNullOrEmpty(question))
            {
                return question;
            }

            Console.WriteLine("String is god damn empty!");
            return question;
        }

        private static int CalculateResult(string msg)
        {
            List<int> possibleInputs = [];


            string currentOutput = "";
            for (int i = 0; i < msg.Length; i++)
            {


                if (Answers.ContainsKey(msg[i].ToString()))
                {
                    int.TryParse(currentOutput, System.Globalization.NumberStyles.Integer, null, out int result);
                    possibleInputs.Add(result);
                    currentOutput = "";
                    continue;
                }

                currentOutput += msg[i].ToString();
            }

            int index = 0;
            int answer = 0;
            for (int i = 0; i < msg.Length; i++)
            {
                if (Answers.ContainsKey(msg[i].ToString()))
                {

                    if (possibleInputs.Count < index + 1)
                        answer = Answers[msg[i].ToString()](possibleInputs[index], possibleInputs[index + 1]);
                }

            }

            return answer;
        }

        static void Main(string[] _args)
        {
            var done = false;
            while (!done)
            {
                // Here's our loop ig
                Console.WriteLine("Welcome to the world's worst calculator");

                string question = GetQuestion();
                CalculateResult(question);
                Console.WriteLine("Uwuau are u done yet");
                var lolz = Console.ReadKey();
                if (Equals("y", lolz))
                {
                    done = true;
                }
            }
        }
    }
}

